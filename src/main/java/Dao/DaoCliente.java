/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.util.List;
import javax.persistence.EntityManager;
import modelo.Cliente;
import configuracion.JPAUtil;
import java.util.ArrayList;

/**
 *
 * @author jroma
 */
public class DaoCliente {

    EntityManager transaccion =  JPAUtil.getEntityMaganerFacory().createEntityManager();

    public boolean agregarCliente(Cliente c) {
        boolean res = false;
        try {
            transaccion.getTransaction().begin();
            transaccion.persist(c);
            transaccion.getTransaction().commit();
            System.out.println("datos registrado con exito");
            return true;
        } catch (Exception ex) {
            System.out.print("error al guardar en " + ex.getMessage());

        }
        return res;

    }

    public boolean EditarCliente(Cliente c, int id) {
        boolean res = false;
        Cliente cliente = new Cliente();
        cliente = transaccion.find(Cliente.class, id);

        if (cliente == null) {
            System.out.print("El cliente no existe en el sistema");

        } else {

            try {

                cliente.setNombre(c.getNombre());
                cliente.setTelefono(c.getTelefono());
                cliente.setEmail(c.getEmail());
                cliente.setApellido(c.getApellido());
                cliente.setEdad(c.getEdad());
                transaccion.getTransaction().begin();
                transaccion.persist(cliente);
                transaccion.getTransaction().commit();

                return true;
            } catch (Exception ex) {
                System.out.print("error al guardar en " + ex.getMessage());

            }
        }

        return res;

    }

    /**
     *
     * @return
     */
    public List<Cliente> obtenerClientes() {

        List<Cliente> arreglo = new ArrayList<>();
        arreglo = transaccion.createQuery("SELECT c FROM Cliente c").getResultList();
        return arreglo;
    }

    public boolean EliminarClientes(int id) {
        boolean res = false;
        Cliente c = new Cliente();
        try {

            c = transaccion.find(Cliente.class, id);
            if (c == null) {
                System.out.print("El cliente no existe en el sistema");
            } else {
                transaccion.getTransaction().begin();
                transaccion.remove(c);
                transaccion.getTransaction().commit();
                res = true;

            }

        } catch (Exception ex) {
            System.out.print("error al eliminar en " + ex.getMessage());

        }
        return res;

    }

}
